{{-- Form --}}
{{-- <form class="form-inline">
  <div class="form-group">
    <label for="inputName" class="sr-only">Name</label>
    <input type="text" class="form-control" id="inputName" placeholder="Name">
  </div>

  <div class="form-group">
    <label for="inputEmail" class="sr-only">Email</label>
    <input type="email" class="form-control" id="inputEmail" value="" placeholder="Enter email">
  </div>

  <button type="submit" class="btn btn-red">Get Updates</button>
</form> --}}

{{-- Select --}}
{{-- <div class="select-wrap">
  <select>
    <option value="placeholder">Select Placeholder</option>
    <option value="one">Option 1</option>
    <option value="two">Option 2</option>
  </select>
</div><!-- /.select-wrap --> --}}

<!-- Begin Mailchimp Signup Form -->
<form action="https://criticalresistance.us4.list-manage.com/subscribe/post?u=b64cbc94231b3bae71ab83686&amp;id=14702ff0eb" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

    <div class="form-group">
      <label for="mce-EMAIL">Email</label>
      <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
    </div>

    <div class="form-group">
      <label for="mce-FNAME">First Name</label>
      <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
    </div>

    <div class="form-group">
      <label for="mce-LNAME">Last Name</label>
      <input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
    </div>
    
    <div class="mc-field-group input-group">
        <strong>Chapters </strong>
        <ul><li><input type="checkbox" value="1" name="group[5][1]" id="mce-group[5]-5-0"><label for="mce-group[5]-5-0">Bay Area</label></li>
    <li><input type="checkbox" value="2" name="group[5][2]" id="mce-group[5]-5-1"><label for="mce-group[5]-5-1">Los Angeles</label></li>
    <li><input type="checkbox" value="16" name="group[5][16]" id="mce-group[5]-5-2"><label for="mce-group[5]-5-2">Portland</label></li>
    <li><input type="checkbox" value="128" name="group[5][128]" id="mce-group[5]-5-3"><label for="mce-group[5]-5-3">New York City</label></li>
    </ul>
    </div>

    <div id="mce-responses" class="clear">
      <div class="response" id="mce-error-response" style="display:none"></div>
      <div class="response" id="mce-success-response" style="display:none"></div>
    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b64cbc94231b3bae71ab83686_14702ff0eb" tabindex="-1" value=""></div>

    <div class="clear"><input type="submit" value="Get Updates" name="subscribe" id="mc-embedded-subscribe" class="btn btn-red"></div>

</form>

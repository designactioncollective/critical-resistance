{{--
  Card C:
  Home -- Text only card
--}}
<article @php(post_class('card-home-c'))>

  <div class="type">
    <?= get_the_term_list( $post->ID, 'resource-type', '<ul><li>', ', </li>', '</li></ul>' ) ?>
  </div>

  <!-- /.type -->
  <div class="card-body">
    <h3 class="card-title"><a href="@permalink">@title</a></h3>
  </div><!-- card-body -->

  <div class="issue">
    <span>@text(Issue Area: )</span><?= get_the_term_list( $post->ID, 'resource-category', '', ', ', '' ); ?>
  </div>


</article><!-- card -->

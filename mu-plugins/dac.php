<?php
/**
 * Plugin Name: Design Action plugins
 * Description: A collection of must-use plugins
 * Author: Design Action Collective
*/


/*
 * Load all mu-plugins
*/

/* Functions */
require_once(dirname(__FILE__) . '/dac/functions/acf_share_options.php');
require_once(dirname(__FILE__) . '/dac/functions/pagination.php');
require_once(dirname(__FILE__) . '/dac/functions/sidebar-menu.php');   

// /* Custom post types and taxonomies */
require_once(dirname(__FILE__) . '/dac/post-types/staff.php');
require_once(dirname(__FILE__) . '/dac/post-types/press-releases.php');
require_once(dirname(__FILE__) . '/dac/post-types/updates.php');
require_once(dirname(__FILE__) . '/dac/post-types/annual-reports.php');
require_once(dirname(__FILE__) . '/dac/post-types/abolitionist.php');
require_once(dirname(__FILE__) . '/dac/post-types/old_abolitionist.php');
require_once(dirname(__FILE__) . '/dac/post-types/resources.php');
require_once(dirname(__FILE__) . '/dac/post-types/in-the-news.php');
require_once(dirname(__FILE__) . '/dac/post-types/old-pages.php');
require_once(dirname(__FILE__) . '/dac/post-types/prisoner-speakout.php');
require_once(dirname(__FILE__) . '/dac/post-types/profiles-in-abolition.php');
require_once(dirname(__FILE__) . '/dac/post-types/projects.php');

/* Advanced custom fields */
require_once(dirname(__FILE__) . '/dac/acf/acf-fields.php');   

/* Shortcodes */
//require_once(dirname(__FILE__) . '/dac/shortcodes/example.php');   


   
<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});


/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);



/*
===============================================================================
*  DAC MODIFICATIONS BELOW
===============================================================================
*/

/**
 * Tell WordPress which template to display the sidebar on
 */

add_filter('sage/display_sidebar', function ($display) {
    static $display;
    $current_page_menu_id =  get_queried_object_id();
    $top_level_menu_id_of_current_page = find_top_level_menu_item($current_page_menu_id);
    $is_about_section = false;
    $is_ourwork_section = false;

    if ($top_level_menu_id_of_current_page == '8320') { // 8320 is menu id for "About" menu item
        $is_about_section = true;
    }
    if ($top_level_menu_id_of_current_page == '8321') { // 8321 is menu id for "Our Work" menu item
        $is_ourwork_section = true;
    }

    $get_page_template = basename(get_page_template());
    $hide_sidebar = false;
    if($get_page_template == 'template-full.blade.php') {
        $hide_sidebar = true;
    }
    isset($display) || $display = in_array(true, [
      // The sidebar will NOT be displayed on all but the following
    //   is_404(),
    //   is_search(),
      is_category(),
      is_tag(),
    //   is_home(),
     //  is_page(),
       ($is_about_section && !$hide_sidebar),
      // ($is_ourwork_section && !$hide_sidebar),
    //   is_page('contact'),
    //   is_page('contact-us'),
      is_post_type_archive(['updates','resources','prisoner-speakout']),
    //   is_singular('tribe_events'),
    //   $get_page_template == 'template-events.blade.php',
      //$get_page_template == 'template-sidebar.blade.php',

    ]);

    return $display;
});


/**
 * Shorten the_excerpt()
 */
function custom_excerpt_length( $length ) {
    return 25; // 25 words
  }
  add_filter( 'excerpt_length',  __NAMESPACE__ . '\\custom_excerpt_length', 999 );

/**
* Remove Posts and Comments
*/
add_action('admin_menu', function() {
    remove_menu_page('edit-comments.php');

});

/**
 * Global data
 */
add_action('after_setup_theme', function() {
    // $links array with social media links
    $sm_links = get_field('social_media_links', 'option');
    sage('blade')->share('links', [
        'facebook'   => 'https://facebook.com/' . $sm_links['facebook'],
        'instagram'  => 'https://instagram.com/' . $sm_links['instagram'],
        // 'linkedin'   => 'https://linkedin.com/' . $sm_links['linkedin'],
        'twitter'    => 'https://twitter.com/' . $sm_links['twitter'],
        'youtube'    => 'https://youtube.com/channel/' . $sm_links['youtube'],
        'flickr'     => 'https://flickr.com/' . $sm_links['flickr'],
        //'googleplus' => 'https://plus.google.com/' . $sm_links['google'],
        //'pinterest'  => 'https://pinterest.com/' . $sm_links['pinterest'],
        //'soundcloud' => 'https://soundcloud.com/' . $sm_links['soundcloud'],
        //'tumblr'     => 'https://tumblr.com/' . $sm_links['tumblr'],
        //'vine'       => 'https://vine.co/' . $sm_links['vine'],
    ]);
});




/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'events','author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Function to alphbetize staff feed
 */
add_action( 'pre_get_posts', function ( $query ) {
    if ( $query->is_main_query() && is_post_type_archive('staff')) {
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );
    }
});

//Add updates to cat and tag archive to enable pagination in S&F pro updates page https://wordpress.stackexchange.com/questions/57439/displaying-category-archive-of-custom-post-types

function query_post_type($query) {
    $post_types = get_post_types();

    if ( is_category() || is_tag()) {

        $post_type = get_query_var('updates');

        if ( $post_type ) {
            $post_type = $post_type;
        } else {
            $post_type = $post_types;
        }

        $query->set('post_type', $post_type);

        return $query;
    }
}

add_filter('pre_get_posts', __NAMESPACE__.'\\query_post_type');

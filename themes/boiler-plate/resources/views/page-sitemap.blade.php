@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    {{-- @include('partials.page-header') --}}
    {{-- @include('partials.content.content-page') --}}
    @php(wp_nav_menu(array( 'menu' => 'primary_navigation', 'echo' => true )))

  @endwhile
@endsection

{{-- Not used but here incase they want to create a slider --}}
{{--  Slick carousel: https://kenwheeler.github.io/slick/  --}}
<section class="row">
  <div class="container slideshow">

    <?php $posts = get_field('slides', 'option');

    global $post;
    if( $posts ): foreach( $posts as $post):
        setup_postdata($post); ?>

        @include('partials.cards.card-slide')

        <?php
         wp_reset_postdata();
        endforeach;
    endif;?>


  </div><!-- /.container -->
</section><!-- /.row -->


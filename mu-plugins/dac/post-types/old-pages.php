<?php

/**
* Old Pages custom post type
*/
// Register Custom Post Type
function custom_post_type_old_pages() {

	$labels = array(
		'name'                  => _x( 'Old Pages', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Old Pages', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Old Pages', 'dac' ),
		'name_admin_bar'        => __( 'Old Pages', 'dac' ),
		'archives'              => __( 'Item Archives', 'dac' ),
		'attributes'            => __( 'Item Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Item:', 'dac' ),
		'all_items'             => __( 'All Items', 'dac' ),
		'add_new_item'          => __( 'Add New Item', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Item', 'dac' ),
		'edit_item'             => __( 'Edit Item', 'dac' ),
		'update_item'           => __( 'Update Item', 'dac' ),
		'view_item'             => __( 'View Item', 'dac' ),
		'view_items'            => __( 'View Items', 'dac' ),
		'search_items'          => __( 'Search Item', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into item', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'dac' ),
		'items_list'            => __( 'Items list', 'dac' ),
		'items_list_navigation' => __( 'Items list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter items list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'Old Pages', 'dac' ),
		'description'           => __( 'Post Type Description', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-tag',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'old-pages', $args );

}
add_action( 'init', 'custom_post_type_old_pages', 0 );

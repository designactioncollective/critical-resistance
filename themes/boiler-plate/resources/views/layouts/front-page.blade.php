<!doctype html>
<html @php(language_attributes())>
  @include('partials.header.head')
  <body @php(body_class())>

    {{-- skip link --}}
    <a href="#content" class="skip-link">@text(Skip to main content)</a>

    {{-- header --}}
    @php(do_action('get_header'))
    @include('partials.header.top-bar')
    @include('partials.header.navigation')

    {{-- content --}}
    <div class="wrap" role="document" tabindex="-1" id="content">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar.sidebar')
          </aside>
        @endif
      </div>
    </div>

    {{-- footer --}}
    @php(do_action('get_footer'))
    @include('partials.footer.footer')
    @php(wp_footer())

  </body>
</html>

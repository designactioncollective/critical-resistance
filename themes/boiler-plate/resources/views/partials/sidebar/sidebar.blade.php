{{-- About section sidebar --}}
@if(is_page())
  <section>
    @if(get_field('sidebar_content','options'))
      <?= get_field('sidebar_content','options')?>
    @endif

    @if(get_field('button_links','option'))
      <ul class="sidebar-buttons">
        @foreach (get_field('button_links','option') as $link )
          <li><a href="<?= get_permalink($link)?>" class="btn btn-red"><?= get_the_title($link)?></a></li>
        @endforeach
      </ul>
    @endif
  </section>

  <section class="sidebar-updates">
    <h3>@text(Updates)</h3>
    @php($args = array(
      'post_type'      => array( 'updates' ),
      'posts_per_page' => 3,
    ))
    @query($args)
      @include('partials.cards.card-sidebar-a')
    @endquery
  </section>

{{-- Updates section sidebar --}}
@elseif(is_post_type_archive('updates') || is_tag() || is_category())
  <section class="sf_filter">
    <h3>@text(Fitler By)</h3>
    <?= do_shortcode('[searchandfilter id="8449"]')?>
  </section>


{{-- Resources section sidebar --}}
@elseif(is_post_type_archive('resources') || is_tax())
  <section class="sf_filter">
  <h3>@text(Fitler By)</h3>
    <?= do_shortcode('[searchandfilter id="8466"]')?>
  </section>


{{-- Prisoner Speakout sidebar --}}
@elseif (is_post_type_archive('prisoner-speakout'))
<?= do_shortcode('[submenu]') ?>
@endif

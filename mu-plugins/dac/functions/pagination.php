<?php
/*
Plugin Name: Design Action - Pagination
Description: Use the function pagination_simple() to add pagination
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/* page navigation*/
// Pagination Numbers 
function pagination($custom_query = '' ) {

  global $wp_query;
  $the_query = $wp_query;   
   
  if ($custom_query) {
     $the_query = $custom_query;  
  }
  
  $total_pages = $the_query->max_num_pages;
  
  if ($total_pages > 1){
  
    $current_page = max(1, get_query_var('paged'));
    
    echo '<div class="page-nav">Page ';
    
    echo paginate_links(array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => 'page/%#%',
        'current' => $current_page,
        'total' => $total_pages,
        'prev_text' => '<i class="fa fa-angle-left"></i>',
        'next_text' => '<i class="fa fa-angle-right"></i>'
      ));
  
    echo '</div>';
    
  }
}
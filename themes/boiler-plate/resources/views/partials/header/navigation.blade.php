<header class="banner">
  <div class="container menu-container">

    {{-- Main Navigation--}}
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu([
          'theme_location' => 'primary_navigation',
          'menu_class'     => 'slimmenu',
        ]) !!}
      @endif

  </div><!--/container-->

  {{-- search --}}
  @include('partials.search-expand-field')
</header>


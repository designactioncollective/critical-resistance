@if (is_post_type_archive('updates') || is_singular('updates') || is_category() || is_tag() )
  {{-- Category --}}
  <section>
    <h2><?php _e( 'Sort by Category' ); ?></h2>
    <form id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">

      <?php
      $args = array(
        'show_option_none' => __( 'Select category' ),
        'show_count'       => 0,
        'orderby'          => 'name',
        'echo'             => 0,
        'post-type'         => 'updates',

      );
      ?>

      <?php $select  = wp_dropdown_categories( $args ); ?>
      <?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
      <?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

      <div class="select-wrap">
        <?php echo $select; ?>
      </div><!-- /.select-wrap -->

      <noscript>
        <input type="submit" value="View" />
      </noscript>

    </form>
  </section>

  {{-- Tag --}}
  <section>
      <h2><?php _e( 'Sort by Tag' ); ?></h2>
      <form id="tag-select" class="tag-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">

        <?php
        $args = array(
          'show_option_none' => __( 'Select tag' ),
          'show_count'      => 0,
          'name'            => 'tag',
	        'id'               => 'tag',
          'orderby'         => 'name',
          'echo'            => 0,
          'taxonomy'        => 'post_tag',
          'value_field'	     => 'slug',

        );
        ?>

        <?php $select  = wp_dropdown_categories( $args ); ?>
        <?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
        <?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

        <div class="select-wrap">
          <?php echo $select; ?>
        </div><!-- /.select-wrap -->

        <noscript>
          <input type="submit" value="View" />
        </noscript>

      </form>
    </section>

  <?php $args = array(
    'type'            => 'monthly',
    'limit'           => '',
    'format'          => 'option',
    'before'          => '',
    'after'           => '',
    'show_post_count' => false,
    'echo'            => 1,
    'order'           => 'DESC',
    'post_type'       => 'updates'
  );?>

  {{-- Date archive select --}}
  <section>
    <h3>@text(Sort by Date)</h3>
    <div class="select-wrap">
      <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
        <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option>
        <?php wp_get_archives( $args ); ?>
      </select>
    </div><!-- /.select-wrap -->
  </section>

  {{-- Search  --}}
  <section>
    <h3>@text(Search by Keyword)</h3>
    <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" class="form-inline">
      <input type="text" name="s" placeholder="Search Updates"/>
      <input type="hidden" name="post_type" value="updates" /> <!-- // hidden 'products' value -->
      <input type="submit" alt="Search" value="Search" />
    </form>
  </section>

@endif

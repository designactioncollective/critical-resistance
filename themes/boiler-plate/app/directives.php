<?php
/**
* Design Action - Blade Directives
*
*/

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Create @vardump() Blade directive
 */
add_action('after_setup_theme', function ($dump) {
    sage('blade')->compiler()->directive('vardump', function ($dump) {
        return "<?php var_dump($dump); ?>";
    });
});

/**
 * Create @posts Blade directive
 * https://discourse.roots.io/t/best-practice-resources-for-blade/8341/25
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('posts', function () {
        return '<?php while(have_posts()) : the_post(); ?>';
    });
});

/**
 * Create @endposts Blade directive
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('endposts', function () {
        return '<?php endwhile; endif; wp_reset_query(); ?>';
    });
});
/**
 * Create @query() Blade directive
 *
 * Usage:
 *
 * @php
 * $args = array(
 *  'author' => 1
 * );
 * @endphp
 *
 * @query($args)
 * <h2>@title</h2>
 * @excerpt
 * @endquery
*/
add_action('after_setup_theme', function ($args) {
    sage('blade')->compiler()->directive('query', function ($args) {
        $output = '<?php
        $bladeQuery = new WP_Query( $args );
        if ( $bladeQuery->have_posts() ) :
            while ( $bladeQuery->have_posts() ) : $bladeQuery->the_post(); ?>';
        return $output;
    });
});

/**
 * Create @endquery Blade directive
 */

add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('endquery', function () {
        return '<?php endwhile;
            else:
                echo "Nothing found.";
            endif;
            wp_reset_postdata();?>';
    });
});

/**
 * Create @title Blade directive
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('title', function () {
        return '<?php the_title(); ?>';
    });
});

/**
 * Create @permalink Blade directive
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('permalink', function () {
        return '<?php the_permalink(); ?>';
    });
});

/**
 * Create @content Blade directive
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('content', function () {
        return '<?php the_content(); ?>';
    });
});

/**
 * Create @excerpt Blade directive
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('excerpt', function () {
        return '<?php the_excerpt(); ?>';
    });
});

/**
 * Create @text Blade directive
 */
add_action('after_setup_theme', function ($text) {
    sage('blade')->compiler()->directive('text', function ($text) {
        return '<?= __("'.$text.'","sage"); ?>';
    });
});


/**
 * Create @pagination Blade directive
 */
add_action('after_setup_theme', function ($custom_query = '') {
    sage('blade')->compiler()->directive('pagination', function ($custom_query) {
        return '<?php pagination(); ?>';
    });
});


/**
 * Create @homesection Blade directive
 */
add_action('after_setup_theme', function ($args) {
    sage('blade')->compiler()->directive('homesection', function ($args) {
        eval("\$params = [$args];");
        list($active_section, $h_link) = $params;
        $output = '<?php
        // check if the flexible content field has rows of data
        $section_control = 0;
        if( have_rows("homepage_sections", "option") ):

          // loop through the rows of data
            while ( have_rows("homepage_sections", "option") ) : the_row();

                // check current row layout
                if( (get_row_layout() == "section") && ('.$active_section.' == $section_control) ):

                  // Section title
                  if(get_sub_field("section_title")) { ?>

                    <h2 class="home-section__header white"><a href="'.$h_link.'"><?= get_sub_field("section_title")?></a></h2>

                  <?php }
                  // check if the nested repeater field has rows of data
                  if( have_rows("tile") ): ?>

                    <div class="row">
                    <?php
                    // loop through the rows of data
                    while ( have_rows("tile") ) : the_row();

                      $image = get_sub_field("image");
                      $link = get_sub_field("link");
                      $text = get_sub_field("text"); ?>
        ';
        return $output;
    });
});

/**
 * Create @endhomesection Blade directive
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('endhomesection', function () {
        $output =  '<?php endwhile; // end while have tiles (repeater)

                    echo "</div>"; // row

                endif; // end if have rows

                endif; // end if layout = section

                $section_control = $section_control + 1;

            endwhile; // end while home page flexible content

            else :

                // no layouts found

            endif; // end if home page flexible content ?>
        ';
        return $output;
    });
});

/**
 * Create @events Blade directive
 *
 * Usage: to show 4 upcoming events
 * @events(4)
 *  @include(partials.content)
 * @endevents
 *
 */
add_action('after_setup_theme', function ($args) {
    sage('blade')->compiler()->directive('events', function ($args) {
        // eval("\$params = [$args];");
        // list($template, $number) = $params;
        $output = '
        <?php global $post;
            $events = tribe_get_events(
                array(
                "posts_per_page" => '. $args .',
                "eventDisplay"   => "list" // only upcoming
                )
            );
            if($events) {
                foreach ( $events as $post ) {
                    setup_postdata( $post );?>';
        return $output;
    });
});

/**
 * Create @endevents Blade directive
 */
add_action('after_setup_theme', function () {
    sage('blade')->compiler()->directive('endevents', function () {
        $output = '<?php }
            wp_reset_postdata();
        } else {
            echo "<p>No upcoming events at this time.</p>";
        }?>';
        return $output;
    });
});


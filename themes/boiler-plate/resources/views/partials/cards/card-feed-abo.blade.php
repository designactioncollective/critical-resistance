{{--
  Card Abolitionist:
  Feed -- Portrait card with image
--}}

<article @php(post_class('card-feed-abo'))>
  <div class="card-feed-abo__img-wrap">
    <a href="@permalink" >
      @if(get_the_post_thumbnail())
        <?php the_post_thumbnail('cover',['class' => 'card-img-top']); ?>
      @else
        <img src="<?= site_url()?>/wp-content/themes/boiler-plate/dist/images/placeholder-cover.png" alt="Placeholder cover image">
      @endif
    </a>

    <div class="card-feed-abo__theme">@title</div>
  </div><!-- /.img-wrap -->

  <div class="card-body">
    
    <div class="card-feed-abo__number">@text(Issue:) <?= get_field('issue_number')?></div>
    <div class="card-feed-abo__date">@text(Issue Date:) <?= get_field('issue_date')?></div>

    <p class="card-text"><?php the_advanced_excerpt('length=30&length_type=words&ellipsis=%26hellip;&allowed_tags='); ?></p>
  </div><!-- card-body -->

</article><!-- card -->

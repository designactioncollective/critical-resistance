{{--
  Title: Call out Box
  Description: Call out box, image with text
  Category: formatting
  Icon: slides
  Keywords: updates news alerts events 
  Mode: edit
  PostTypes: page
  SupportsMode: false
  SupportsMultiple: false
--}}


@php
$image = get_field('image');
$title = get_field('title');
$content = get_field('content');
$link = get_field('link');
@endphp

<section data-{{ $block['id'] }} class="{{ $block['classes'] }} card-call-out">
    @if ($link) <a href="<?= $link['url']?>" >@endif
        @if($image) <?= wp_get_attachment_image( $image, 'rectangle');?> @endif
    @if ($link) </a> @endif

    <div class="card-body">
        <h3 class="card-title h4">
            @if ($link) <a href="<?= $link['url']?>" > @endif
                <?= $title ?> 
            @if ($link) </a> @endif
        </h3>
        <?= $content?>
    </div><!-- card-body -->
    
</section>

@extends('layouts.front-page')

@section('content')
  @include('partials.home.home-feature')

  @include('partials.home.home-section-1')
  @include('partials.home.home-section-2')
  @include('partials.home.home-section-3')
  @include('partials.home.home-section-4')
  @include('partials.home.home-section-5')

@endsection

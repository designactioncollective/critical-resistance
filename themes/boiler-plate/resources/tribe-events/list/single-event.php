<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @version 4.6.3
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// The address string via tribe_get_venue_details will often be populated even when there's
// no address, so let's get the address string on its own for a couple of checks below.
$venue_address = tribe_get_address();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

?>

<article class='card-feed-e' >

  <?php echo tribe_event_featured_image( null, 'rectangle' ); ?>

  <div class="card-body">
    <!-- Event Title -->
    <?php do_action( 'tribe_events_before_the_event_title' ) ?>
    <h3 class="card-title"><a href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark"><?php the_title() ?></a></h3>
    <?php do_action( 'tribe_events_after_the_event_title' ) ?>

    <!-- Event Meta yel-->
    <?php do_action( 'tribe_events_before_the_meta' ) ?>

    <div class="card-text">
        <div class="post-meta">
            <div class="author <?php echo esc_attr( $has_venue_address ); ?>">

                <!-- Schedule & Recurrence Details -->
                <div class="schedule-details">
                    <?php echo tribe_events_event_schedule_details() ?>
                </div>

            </div>
        </div><!-- .tribe-events-event-meta -->

        <?php do_action( 'tribe_events_after_the_meta' ) ?>

        <!-- Event Content -->
        <?php do_action( 'tribe_events_before_the_content' );
        echo '<p>';
        the_advanced_excerpt('length=55&length_type=words&no_custom=1&ellipsis=%26hellip;&exclude_tags=img,p,strong');
        echo '</p>'; ?>

        <?php //echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?>
        <!-- <a href="<?php //echo esc_url( tribe_get_event_link() ); ?>" class="tribe-events-read-more" rel="bookmark"><?php // esc_html_e( 'Find out more', 'the-events-calendar' ) ?> &raquo;</a> -->
        <?php do_action( 'tribe_events_after_the_content' );?>

    </div><!-- card-text -->

  </div><!-- card-body -->

</article><!-- card -->

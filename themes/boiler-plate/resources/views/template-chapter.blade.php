{{--
  Template Name: CR Chapter Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    @include('partials.content.content-page')
  @endwhile


  {{-- Our Work / Engage With Us --}}
  <div class="row chapters__lower">

    {{-- Our Work --}}
    <div class="col-md-8">
      <h2>@text(Our Work)</h2>
      <?php
      $tag = $post->post_name; // current chapter page name matches tag
      $args = array(
        'post_type'         => array( 'projects' ),
        'tag'               => $tag,
        'posts_per_page'    => 3
      );

      $query = new WP_Query( $args );

      if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post();
      ?>
        <div class="row">
          <div class="col">
            @include('partials.cards.card-feed-a')
          </div>
        </div><!-- /.row -->
      <?php
        }
      } else {
        echo "No current projects found.";
      }
      wp_reset_postdata();
      ?>
    </div><!-- /.col-md-8 -->

    {{-- Engage Links --}}
    <div class="col-md-4">
      <?php
       if( have_rows('engage_links') ): ?>
          <h2 class="h4">@text(Engage With Us)</h2>
            <ul class="engage-links">
            <?php
            while ( have_rows('engage_links') ) : the_row();
              $link = get_sub_field('engage_link');?>
              <li><a href="<?= $link['url']?>" target="<?= $link['target']?>" class="btn btn-red btn-sidebar"><?= $link['title']?></a></li>
              <?php
            endwhile;
        echo '</ul>';
      endif;
      ?>

    </div><!-- /.col-md-4 -->


  </div><!-- /.row -->

@endsection

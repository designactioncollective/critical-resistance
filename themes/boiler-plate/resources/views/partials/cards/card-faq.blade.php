{{--
  Card FAQ:
--}}
<article @php(post_class('card-faq'))>
  <div class="card-body">

    <h4 class="faq-question">
        <a role="button" data-toggle="collapse" href="#<?php echo 'faq-'. $counter;?>-answer" aria-expanded="false" aria-controls="collapseExample3" class="collapsed">@title <span class="see-more"></span></a>
    </h4>

    <div class="collapse" id="<?php echo 'faq-'. $counter; ?>-answer">
        <div class="faq-answer">
            @content
        </div>
    </div><!-- collapse -->

  </div>  {{-- card-body --}}
</article><!-- card -->

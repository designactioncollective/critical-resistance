<footer style="background-image: url(<?= get_field('donate_background_image_footer', 'option')?>)">
  {{--  Donate section  --}}
  <section class="footer__donate">
    <div class="container">
      <h2>{!! $donation_headline !!}</h2>
      <p>{!! $donation_text!!}</p>
      <a href="{!! $donation_button['url']!!}" target="{!! $donation_button['target']!!}" class="btn-support">
        <span>{!! $donation_button['title']!!}</span>
      </a>
    </div><!-- /.container -->
  </section>

  <section class="footer__lower">
    <div class="container">

      <div class="footer__lower_a">
        {{--  footer logo  --}}

        <a class="footer__logo" href="{{ home_url('/') }}" rel='home'>
          <img src='@asset("images/logo.png")' class='img-fluid'>
        </a>

        {{-- donate & social media  --}}
        <div class="__lower-a--right">
          <a href="{!! $donate['url'] !!}" target={!! $donate['target']!!} class="btn-donate"><span>{!! $donate['title']!!}</span></a>
          @include('partials.social-media')
        </div><!-- /.__lower-a--right -->

      </div><!-- /.footer__lower_a -->


      {{--  footer menu  --}}
      @if (has_nav_menu('footer_navigation'))
        {!!wp_nav_menu([
          'theme_location'  => 'footer_navigation',
          'menu_class'      => 'nav footer__nav',
          'menu_id'         => '',
          'depth'           => 1
        ]) !!}
      @endif

      {{--  contact info  --}}
      <ul class="colophon">
        <li>{!! $email !!}</li>
        <li>{!! $phone !!}</li>
        <li>{!! $address !!}</li>
        <li>&copy; <?php echo date("Y"); ?></li>
        <li><a href="/site-credits">@text(Site Credits)</a></li>
      </ul>


    </div><!--/container-->
  </section>

  @include('partials.modal')

</footer>

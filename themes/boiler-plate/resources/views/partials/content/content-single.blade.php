<article @php(post_class())>

  @include('partials/entry-meta')

  @include('partials/entry-meta-after')

  <div class="entry-content">
    @php(the_content())
  </div>

</article>
{{--
  Card ACF:
  Portrait card with image
--}}
<article class='card-acf'>
  <a href="<?= $link['url']?>" class="img-link">
    <?= wp_get_attachment_image( $image, 'rectangle' ); ?>
  </a>

  <div class="card-body">
    <h3 class="card-title"><a href="<?= $link['url']?>"><?= $link['title']?></a></h3>
    {{-- <p>//echo $text</p> --}}
  </div><!-- card-body -->

</article><!-- card -->

{{-- prisoner-speakout --}}
@if(
  is_post_type_archive('prisoner-speakout') || 
  is_tax('speakout-category') ||   
  is_singular('prisoner-speakout')
)
  <?= get_the_term_list( $post->ID, 'speakout-category', '<ul class="category-list meta">', ', ', '</ul>' ); ?>
@endif
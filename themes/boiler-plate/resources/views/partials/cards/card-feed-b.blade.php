{{--
  Card B:
  Feed -- Landscape card with image
--}}

<article @php(post_class('card-feed-b'))>

  <a href="@permalink" ><?php the_post_thumbnail('square',['class' => 'card-img-top']); ?></a>

  <div class="card-body">
    @if (get_post_type($post->ID) == 'member-resource')
      @php(the_terms( $post->ID, 'resource_type', '', ', ' ))
    @endif


    <h3><a href="@permalink">@title</a></h3>
    
    @if (get_post_type() == 'projects')
      <div class="tags"><?= get_the_tag_list( 'Tags: ', ', ', ''); ?></div>
    @else 
      @include('partials/entry-meta')
    @endif

    <p class="card-text">@excerpt</p>

  </div><!-- card-body -->

</article><!-- card -->

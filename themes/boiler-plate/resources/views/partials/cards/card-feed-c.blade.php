{{--
  Card C:
  Feed -- Resources
--}}
<article @php(post_class('card-feed-c'))>


  <div class="type--yellow">
    <?= get_the_term_list( $post->ID, 'resource-type', '<ul><li>', ', </li>', '</li></ul>' ) ?>
  </div>

  <div class="card-body">
    <h3 class="card-title h4"><a href="@permalink">@title</a></h3>
    
    <div class="resource-meta">
      <span>@text(Issue Area: )</span> <?= get_the_term_list( $post->ID, 'resource-category', '', ', ', '' ); ?>
    </div>

    <div class="resource-meta">
      <span>@text(Keywords: )</span> <?= get_the_term_list( $post->ID, 'resource-tag', '', ', ', '' ); ?>
    </div>
    

  </div><!-- card-body -->

</article><!-- card -->

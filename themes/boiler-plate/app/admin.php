<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});


/*
===============================================================================
*  DAC MODIFICATIONS BELOW
===============================================================================
*/

/**
 * Admin styles
 */
add_action('admin_enqueue_scripts', function () {
    wp_enqueue_style('sage/admin.css', asset_path('styles/admin.css'), false, null);
    wp_enqueue_script('sage/admin.js', asset_path('scripts/admin.js'), ['jquery'], null, true);
}, 100);

/**
 * Logo select in customizer
 */
add_action( 'customize_register', function(\WP_Customize_Manager $wp_customize ) {
    $wp_customize->add_section( 'themeslug_logo_section' , array(
        'title'       => __( 'Logo', 'themeslug' ),
        'priority'    => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ) );

    // START Internal Page Logo
    $wp_customize->add_setting( 'themeslug_logo_internal' );
    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'themeslug_logo_internal', array(
        'label'    => __( 'Internal Page Logo', 'themeslug' ),
        'section'  => 'themeslug_logo_section',
        'settings' => 'themeslug_logo_internal',
    ) ) );
    // END Internal Page Logo

    // START Home Logo
    $wp_customize->add_setting( 'themeslug_logo_home' );
    $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'themeslug_logo_home', array(
        'label'    => __( 'Home Logo', 'themeslug' ),
        'section'  => 'themeslug_logo_section',
        'settings' => 'themeslug_logo_home',
    ) ) );
    // END Home Logo
});


/**
 * Login Page Styles
 */
add_action('login_enqueue_scripts', function () {
    wp_enqueue_style('sage/login.css', asset_path('styles/login.css'), false, null);
    wp_enqueue_script('sage/login.js', asset_path('scripts/login.js'), ['jquery'], null, true);
}, 100);

/**
 * Login Page Logo Link
 */

add_filter( 'login_headerurl', function() {
    return home_url();
});

add_filter( 'login_headertext', function() {
    return  get_bloginfo();
});

add_action( 'admin_notices', function() { 
    $current_post_type = get_current_screen()->post_type; 
    $acf_field_name = $current_post_type . '-page-content';
    if(is_post_type_archive() && get_field($acf_field_name, 'option')) {?>

    <div class="notice notice-info is-dismissible">
        <?= get_field($acf_field_name, 'option'); ?>
        <a href="<?= '/wp-admin/edit.php?post_type=' . $current_post_type .'&page=acf-options-'. $current_post_type .'-page-content'?>"><?php _e( 'Edit content at top of ' . $current_post_type . ' feed.', 'sage' ); ?></a> 
    </div>

    <?php } 
});


// Remove legacy WordPress Link Manager
update_option( 'link_manager_enabled', 0 );
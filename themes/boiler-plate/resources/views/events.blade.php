{{--
  Template Name: Events
--}}

@extends('layouts.app')

@section('content')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  {{-- work-around for events cal pro template --}}
  @if (is_post_type_archive('tribe_events') && !is_single())
    @include('partials.events')
  @else
    @while (have_posts()) @php(the_post())
      @include('partials.content.content-'.get_post_type())
    @endwhile
  @endif

  @pagination

@endsection

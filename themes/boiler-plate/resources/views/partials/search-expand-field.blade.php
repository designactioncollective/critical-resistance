{{-- expanded search field --}}
<div class="container search-bar-expanded">
  <div class="row">

    <div class="collapse" id="h-search-accordion">
      <div class="h-search-form">
        {!! get_search_form(false) !!}
      </div>
    </div><!-- collapse -->

  </div><!-- /.row -->
</div><!-- /.container -->

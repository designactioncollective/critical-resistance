{{--  Home section  --}}
<section class="home-section--1">
  <div class="container">
    <div class="row">

      @homesection(0, '/our-chapters')
        <div class="col-md-6 col-lg-3">
          @include('partials.cards.card-acf')
        </div>{{-- col --}}
      @endhomesection

    </div><!-- /.row -->
  </div><!-- /.container -->

  <div class="home-icon">
    @if(get_field('icon','option'))
      <?= wp_get_attachment_image( get_field('icon','option'), 'square' ); ?>
    @endif
  </div>

</section>


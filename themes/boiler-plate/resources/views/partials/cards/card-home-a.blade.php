{{--
  Card A:
  Home -- Portrait card with image
--}}
<article @php(post_class('card-home-a'))>

  <a href="@permalink" ><?php the_post_thumbnail('square',['class' => 'card-img-top']); ?></a>

  <div class="card-body">
    <h3 class="card-title"><a href="@permalink">@title</a></h3>
    @include('partials/entry-meta')
    <p class="card-text">@excerpt</p>
    <!-- <a href="#" class="btn btn-primary">Card Button</a> -->
    <!-- <a href="#" class="card-link">Card link</a> -->

  </div><!-- card-body -->

</article><!-- card -->

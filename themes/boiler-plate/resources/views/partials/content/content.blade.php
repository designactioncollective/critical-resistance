@if(is_post_type_archive('staff') )
  @include('partials.cards.card-staff')

@elseif(is_post_type_archive('annual-reports'))
  @include('partials.cards.card-feed-annual-r')

@elseif(is_post_type_archive('abolitionist'))
  @include('partials.cards.card-feed-abo')

@elseif(is_post_type_archive('press-releases'))
  @include('partials.cards.card-feed-pr')

@elseif(is_post_type_archive('resources'))
  @include('partials.cards.card-feed-c')

@else
  @include('partials.cards.card-feed-a')

@endif


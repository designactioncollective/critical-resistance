{{-- Staff Card: --}}
<article @php(post_class('card-staff'))>

  <div class="staff-info">
    <h2 class="h4">@title</h2>
    <p class="position"><?= get_field('position');?></p>
    @if(get_field('email'))
      <a href="mailto:<?= get_field('email')?> " class="email"><?= get_field('email');?></a>
    @endif
    @if(get_field('phone_number'))
      <p class="phone"><?= get_field('phone_number') . ' | Ext. ' . get_field('extension');?></p>
    @endif
  </div>

  <div class="card-body">
    <div class="card-text">
      <p><?=get_field('lead_paragraph')?></p>
      <div class="collapse" id="<?='bio-'. $post->ID; ?>">
        <?=get_field('secondary_paragraphs')?>
      </div><!-- collapse -->
    </div>{{-- card-text --}}

<!--   <div class="read-bio">
      <a role="button" data-toggle="collapse" href="#<?//= 'bio-'. $post->ID;?>" aria-expanded="false" aria-controls="collapseExample3" class="collapsed"><span class="see-more"></span></a>
    </div> --> {{--read-bio --}}
  </div><!-- card-body -->



</article><!-- card -->

<div class="social-icons">
  @php
  $icons = array(
    array(
      'fa-icon' => 'facebook',
      'url'     =>  $links['facebook'],
      'title'   => 'Follow us on Facebook',
      'addition'=> ''
    ),
    array(
      'fa-icon' => 'twitter',
      'url'     => $links['twitter'],
      'title'   => 'Follow us on Twitter',
      'addition'=> ''
    ),

    array(
      'fa-icon' => 'instagram',
      'url'     => $links['instagram'],
      'title'   => 'Follow us on Instagram',
      'addition'=> ''
    )

  );
  foreach ($icons as $icon) :
    echo '<a href="'.$icon['url'].'" target="_blank" title="'.$icon['title'].'"><i class="fa fa-'.$icon['fa-icon'].'" aria-hidden="true"></i>';
    if ($icon['addition']) echo '<small aria-hidden="true" translate="no">'.$icon['addition'].'</small>';
    echo '</a>';
  endforeach;
  @endphp
</div>

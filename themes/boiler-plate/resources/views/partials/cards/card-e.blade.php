{{--
  Card Event:
--}}
<article @php(post_class('card-e'))>


  <div class="card-body">
    <time class="date">
        <?= tribe_get_start_date ( $post, true,'F j, Y @ g:i a' );?>
    </time>

    <h3><a href="@permalink">@title</a></h3>

    {{-- <p class="card-text">@excerpt</p> --}}

  </div><!-- card-body -->

</article><!-- card -->

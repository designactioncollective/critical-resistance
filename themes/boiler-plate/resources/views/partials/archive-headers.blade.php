@php
  $page_content = get_post_type() . '-page-content';
  $featured_image = get_post_type() . '_page_featured_image';

  // Top featured image (staff only)
  if(get_field($featured_image,'option')) {
    echo '<div class="lead-image">';
    echo wp_get_attachment_image(get_field($featured_image,'option'), 'full');
    echo '</div>';
  }

  // Top content for achives
  if(get_field($page_content,'option')) {
    the_field($page_content,'option');
  }
@endphp



<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);



/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});


/*
===============================================================================
*  DAC MODIFICATIONS BELOW
===============================================================================
*/

/**
 * Register sidebars
 */
// add_action('widgets_init', function () {
//     $config = [
//         'before_widget' => '<section class="widget %1$s %2$s">',
//         'after_widget'  => '</section>',
//         'before_title'  => '<h3>',
//         'after_title'   => '</h3>'
//     ];
    // register_sidebar([
    //     'name'          => __('Primary', 'sage'),
    //     'id'            => 'sidebar-primary'
    // ] + $config);
    // register_sidebar([
    //     'name'          => __('Footer', 'sage'),
    //     'id'            => 'sidebar-footer'
    // ] + $config);
// });

/**
* Custom Thumbnails
*/

set_post_thumbnail_size(300, 200, true);

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'slide', 1500,655, true );
    add_image_size( 'banner', 1500,514, true );
    add_image_size( 'square', 300,300, true );
    add_image_size( 'rectangle', 380,236, true );
    add_image_size( 'cover', 300,381, true );
}


/**
* ACF Options Page
*/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
        'capability'    => 'edit_posts',
        'icon_url'      => 'dashicons-admin-settings',
        'position'      => 45,
        'redirect'      => true
        //'redirect'      => false
    ));


	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Options',
		'menu_title'	=> 'Footer Options',
		'parent_slug'	=> 'theme-general-settings',
	));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Media Settings',
		'menu_title'	=> 'Social Media',
		'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Donation Link',
		'menu_title'	=> 'Donation Link',
		'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Default Page Banners',
		'menu_title'	=> 'Default Page Banners',
		'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Sidebar',
		'menu_title'	=> 'Sidebar',
		'parent_slug'	=> 'theme-general-settings',
    ));

    // acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Footer Settings',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'theme-general-settings',
    // ));

    acf_add_options_page(array(
		'page_title' 	=> 'Home Page',
		'menu_title'	=> 'Home Page',
		'menu_slug' 	=> 'home-page-settings',
        'capability'    => 'edit_posts',
        'icon_url'      => 'dashicons-admin-home',
        'position'      => 40,
        'redirect'      => true
        //'redirect'      => false
    ));


    // Add lead content to In the News
    acf_add_options_sub_page(array(
        'page_title'  => 'In the News Page Content',
        'menu_title'  => 'In the News Page Content',
        'parent_slug' => 'edit.php?post_type=in-the-news',
    ));

    // Add lead content to Updates
    acf_add_options_sub_page(array(
        'page_title'  => 'Updates Page Content',
        'menu_title'  => 'Updates Page Content',
        'parent_slug' => 'edit.php?post_type=updates',
    ));

    // Add lead content to Abolitionist
    acf_add_options_sub_page(array(
        'page_title'  => 'Abolitionist Page Content',
        'menu_title'  => 'Abolitionist Page Content',
        'parent_slug' => 'edit.php?post_type=abolitionist',
    ));

    // Add lead content to Prisoner Speakout
    acf_add_options_sub_page(array(
        'page_title'  => 'Prisoner Speakout Page Content',
        'menu_title'  => 'Prisoner Speakout Page Content',
        'parent_slug' => 'edit.php?post_type=prisoner-speakout',
    ));

    // Add lead content to Profiles in Abolition
    acf_add_options_sub_page(array(
        'page_title'  => 'Profiles in Abolition Page Content',
        'menu_title'  => 'Profiles in Abolition Page Content',
        'parent_slug' => 'edit.php?post_type=profiles-abolition',
    ));

    // Add lead content to Annual Report
    acf_add_options_sub_page(array(
        'page_title'  => 'Annual Report Page Content',
        'menu_title'  => 'Annual Report Page Content',
        'parent_slug' => 'edit.php?post_type=annual-reports',
    ));

    // Add lead content to Staff Page
    acf_add_options_sub_page(array(
        'page_title'  => 'Staff Page Content',
        'menu_title'  => 'Staff Page Content',
        'parent_slug' => 'edit.php?post_type=staff',
    ));

    // Add lead content to Resources Page
    acf_add_options_sub_page(array(
        'page_title'  => 'Resources Page Content',
        'menu_title'  => 'Resources Page Content',
        'parent_slug' => 'edit.php?post_type=resources',
    ));

}


/**
 * Register navigation menus
 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
 */
register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'footer_navigation' => __('Footer Navigation', 'sage'),
]);


// Remove tags support from posts
// add_action('init', function() {
//     unregister_taxonomy_for_object_type('post_tag', 'post');
// });

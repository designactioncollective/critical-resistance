{{--
  Sidebar Card A:
--}}
<article @php(post_class('card-sidebar-a'))>

  <div class="card-body">
  
    @include('partials/entry-meta')
    <h3 class="card-title"><a href="@permalink">@title</a></h3>
    <?= get_the_tag_list('', ', '); ?>

  </div><!-- card-body -->

</article><!-- card -->

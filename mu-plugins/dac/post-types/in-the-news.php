<?php

/**
*  In the News custom post type
*/
// Register Custom Post Type
function custom_post_type_in_the_news() {

	$labels = array(
		'name'                  => _x( 'In The News', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'In The News', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'In The News', 'dac' ),

	);
	$args = array(
		'label'                 => __( 'In The News', 'dac' ),
		'description'           => __( 'Post Type Description', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-welcome-widgets-menus',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'in-the-news', $args );

}
add_action( 'init', 'custom_post_type_in_the_news', 0 );

{{--
  Card A: All content
  Feed -- Portrait card with image
--}}
<article @php(post_class('card-feed-pr'))>

  <a href="@permalink" ><?php the_post_thumbnail('square',['class' => 'card-img-top']); ?></a>

  <div class="card-body">
    <h3 class="card-title"><a href="@permalink">@title</a></h3>
    
    @include('partials/entry-meta')

    <?php 
    
    $content_post = get_post($post->ID);
    $content = $content_post->post_content;
    //$content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
    ?>


  </div><!-- card-body -->

</article><!-- card -->

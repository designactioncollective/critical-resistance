{{--
  Card A:
  Feed -- Portrait card with image
--}}
<article @php(post_class('card-feed-a'))>

  @if(get_the_post_thumbnail())
    <a href="@permalink" class="img-link"><?php the_post_thumbnail('rectangle',['class' => 'card-img-top']); ?></a>
  @endif

  <div class="card-body">
    <h3 class="card-title h4"><a href="@permalink">@title</a></h3>
    
    @include('partials/entry-meta')

    <p class="card-text"><?php the_advanced_excerpt('length=30&length_type=words&ellipsis=%26hellip;&allowed_tags='); ?></p>

    @include('partials/entry-meta-after')
    

  </div><!-- card-body -->

</article><!-- card -->

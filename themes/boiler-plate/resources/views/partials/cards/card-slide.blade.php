{{--
Slide
--}}
@php
$link  = get_sub_field('slide_link', 'option');
$text  = get_sub_field('slide_text', 'option');
$title = get_sub_field('slide_title', 'option');
$image = get_sub_field('slide_image', 'option');
if($link):
  $link_url = $link['url'];
  else:
  $link_url = '#';
endif;
@endphp

<div class="slide">

    <a href="{!! $link_url !!}" class="slide__image" >
      <?php echo wp_get_attachment_image( $image, 'slide' ); ?>
    </a>

    <div class="container">
      <h2 class="home-section__header large">
        <a href="{!! $link_url !!}">
          {!! $title !!}
        </a>
      </h2>
      <p>{!! $text !!}</p>
    </div>{{-- container --}}

</div><!-- slide -->

{{--
  Title: Chapter Page News Section
  Description: Chapter Updates, Alerts, and Events
  Category: widgets
  Icon: images-alt2
  Keywords: updates news alerts events 
  Mode: edit
  Align: left
  PostTypes: page
  SupportsMode: false
  SupportsMultiple: false
--}}

<section data-{{ $block['id'] }} class="{{ $block['classes'] }} chapter-news-section">
    <div class="row">

    {{--  --}}
    {{-- Updates --}}
    {{--  --}}    
    <?php 
    global $post;
    $tag = $post->post_name;
    $updates = $alerts = $events = '';
    $displayed_update_ids = [];

    if(get_field('automatic_or_manual_updates') == 'auto') {
        $args = array(
            'post_type'         => array( 'updates' ),
            'tag'               => $tag,
            'posts_per_page'    => 2
        );
        $updates = new WP_Query( $args );
        $updates = $updates->posts;

    } else { // manual    
        $updates = get_field('updates');
    }?>

    @if($updates)  
        <div class="col chapter-news-col">
            <div class="card-chapter-wrap">
                <h2 class="h4">@text(Chapter Updates)</h2>
                @foreach ($updates as $post )
                    <?php setup_postdata($post); 
                    array_push($displayed_update_ids,$post->ID);?>    
                    @include('partials.cards.card-feed-text-only')
                @endforeach
            </div><!-- /.card-chapter-wrap -->
        </div><!-- /.col-->
    @endif 
    
    {{--  --}}
    {{-- Alerts --}}
    {{--  --}}
    <?php 
    if(get_field('automatic_or_manual_alerts') == 'auto') {
        $args = array(
            'post_type'         => array( 'updates' ),
            'tag'               => $tag,
            'category_name'     => 'action-alerts',
            'posts_per_page'    => 2,
            'post__not_in'      => $displayed_update_ids,
        );
        $alerts = new WP_Query( $args );
        $alerts = $alerts->posts;

    } else { // manual    
        $alerts = get_field('alerts');
    } ?>

    @if($alerts)
        <div class="col chapter-news-col">

            <div class="card-chapter-wrap">
            <h2 class="h4">@text(Chapter Alerts)</h2>
                @foreach ($alerts as $post )
                    <?php setup_postdata($post); ?>    
                    @include('partials.cards.card-feed-text-only')
                @endforeach
            </div><!-- /.card-chapter-wrap -->

        </div><!-- /.col -->
    @endif

    {{--  --}}
    {{-- Events --}}
    {{--  --}}
    <?php
    if(get_field('automatic_or_manual_events') == 'auto') {

        $events = tribe_get_events( [ 
            'posts_per_page' => 2, 
            'start_date'     => 'now',
            'tag'            => $tag,

        ] );

    } else { // manual    
        $events = get_field('events');
    }
    ?>
    @if ($events)    
        <div class="col chapter-news-col">
            
            <div class="card-chapter-wrap">
                <h2 class="h4">@text(Upcoming Events)</h2>
                @foreach ($events as $post )
                    <?php setup_postdata($post); ?>    
                    @include('partials.cards.card-feed-text-only')
                @endforeach
            </div><!-- /.card-chapter-wrap -->

        </div><!-- /.col -->
    @endif

    </div><!-- /.row -->
    
    <?php wp_reset_postdata(); ?> 

</section>

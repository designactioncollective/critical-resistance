{{-- https://simplesharingbuttons.com/ --}}
<ul class="share-buttons">
  <li>Share this</li>
  <li>
    <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fcriticalresistance.org&quote=D" title="Share on Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&quote=' + encodeURIComponent(document.URL)); return false;">
    <i class="fa fa-facebook" aria-hidden="true"></i>
    </a>
  </li>
  
  <li>
    <a href="https://twitter.com/intent/tweet?source=https%3A%2F%2Fcriticalresistance.org&text=D:%20https%3A%2F%2Fcriticalresistance.org&via=C_Resistance" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20'  + encodeURIComponent(document.URL)); return false;">
    <i class="fa fa-twitter" aria-hidden="true"></i>
    </a>
  </li>

  <li>
    <a href="http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fcriticalresistance.org&description=" target="_blank" title="Pin it" onclick="window.open('http://pinterest.com/pin/create/button/?url=' + encodeURIComponent(document.URL) + '&description=' +  encodeURIComponent(document.title)); return false;">
    <i class="fa fa-pinterest" aria-hidden="true"></i>
    </a>
  </li>

  <li>
    <a href="http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fcriticalresistance.org&title=D&summary=&source=https%3A%2F%2Fcriticalresistance.org" target="_blank" title="Share on LinkedIn" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' +  encodeURIComponent(document.title)); return false;">
    <i class="fa fa-linkedin" aria-hidden="true"></i>
    </a>
  </li>

  <li>
    <a href="mailto:?subject=D&body=:%20https%3A%2F%2Fcriticalresistance.org" target="_blank" title="Send email" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' +  encodeURIComponent(document.URL)); return false;">
    <i class="fa fa-envelope" aria-hidden="true"></i>
    </a>
  </li>
</ul>
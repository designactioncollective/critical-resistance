{{-- profiles in abolition --}}
@if(get_field('date_of_event')) 
  <p class="event-date">@text(Event Date:  )<?= get_field('date_of_event') ?></p>

{{-- in the news  & press release--}}
@elseif(is_post_type_archive(['in-the-news','press-releases']))

  @if(get_field('publish_date'))
    <time><?= get_field('publish_date');?></time>
  @endif
  
  @if(get_field('author_name'))
    <span class="news-author"><?= 'by ' . get_field('author_name') ?></span>
  @endif

  {{-- press release --}}
  @if(get_field('media_contact'))
    <p class="media-contact"><span>@text(Media Contact: )</span> <?= get_field('media_contact') ?></p>
  @endif

@else
  <time class="date" datetime="{{ get_post_time('c', true) }}"><?= __('Posted on ')?>{{ get_the_date() }}</time>
@endif


{{-- prisoner speakout --}}
@if(get_field('name_of_creator'))
  <p class="name-creator"><?= get_field('name_of_creator') ?></p>
@endif

{{-- <p class="byline author vcard"> --}}
  {{-- {{ __('By', 'sage') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn"> --}}
    {{-- {{ get_the_author() }} --}}
  {{-- </a> --}}
{{-- </p> --}}


{{-- @if (is_post_type_archive( array('staff','board')))
    @php
      echo get_post_meta($post->ID, 'position', true);
      if(get_post_meta($post->ID, 'phone_number')) {
        echo ' / ';
        echo get_post_meta($post->ID, 'phone_number', true);
      }
    @endphp
@endif --}}

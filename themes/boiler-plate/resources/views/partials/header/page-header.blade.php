{{-- <header> tag is opened in page-banner-img.blade.php --}}

  @include('partials.header.page-banner-img')
      
  <div class="page-banner">
    <div class="container">

      <div class="row">
        {{-- @php(dac_the_breadcrumbs('primary_navigation')) --}}

        {{-- <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/"> --}}
            <?php //if(function_exists('bcn_display')){ bcn_display(); }?>
        {{-- </div> --}} 

      </div><!-- row -->

      <div class="row page-title">
        <div class="col-md-9"><h1>{!! App::title() !!}</h1></div> 
        <div class="col-md-3">@include('partials.share-this')</div> 
      </div><!-- /.row -->

    </div><!-- /.conatainer -->
  </div><!-- /.page-banner -->

</header><!--page-header-->

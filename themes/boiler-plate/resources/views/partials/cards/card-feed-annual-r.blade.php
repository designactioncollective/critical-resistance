{{--
  Card Annual reports:
--}}
<article @php(post_class('card-feed-annual-r'))>

  <a href="@permalink" ><?php the_post_thumbnail('cover',['class' => 'card-img-top']); ?></a>

  <div class="card-body">
    <h3 class="card-title h4"><a href="@permalink">@title</a></h3>
    <p class="card-text"><?php the_advanced_excerpt('length=30&length_type=words&ellipsis=%26hellip;&allowed_tags='); ?></p>
  </div><!-- card-body -->

</article><!-- card -->

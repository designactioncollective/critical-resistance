@extends('layouts.app')

@section('content')
  <?php  
  $grid = false;
  $grid_wrap = false;
  $is_press_release_first_post = false;?>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @if(is_archive())
    @include('partials.archive-headers')
  @endif

  @php 
    if(is_post_type_archive(['abolitionist'])) { // true for all pages with grid
      $grid_wrap = true;
      echo '<h2>' . __('Issues') . '</h2>';
      echo '<div class="grid-wrap grid-wrap__' . get_post_type() . '">';
    }
    if(is_post_type_archive(['annual-reports','abolitionist'])) { // true for all pages with grid
      $grid = true;
      echo '<div class="inner-grid inner-grid__' . get_post_type() . '">';
    }      
    if(is_post_type_archive('press-releases')) {
      $is_press_release_first_post = true; 
    }
  @endphp

  @if(is_post_type_archive('prisoner-speakout') || is_tax('speakout-category'))
    <div class="select-wrap select-wrap--full-width"><?= do_shortcode('[searchandfilter id="8400"]') ?></div>
    <?php //= do_shortcode('[searchandfilter id="8400" show="results"]') ?>
  @endif


  @while (have_posts()) @php(the_post())
    @if($is_press_release_first_post) 
      @include('partials.content.content-full-'.get_post_type())
      <?php $is_press_release_first_post = false; ?>
      <h2 class="past-press h4">@text(Past Press Releases)</h2>
    @else
      @include('partials.content.content-'.get_post_type())
    @endif
  @endwhile



  @if($grid) 
    </div> {{-- end of grid --}}
  @endif 
  


  {{-- abolitionist sidebar --}}
  @if (is_post_type_archive('abolitionist') )
    @include('partials.sidebar.sidebar-abolitionist')
  @endif

  @if($grid_wrap) 
    </div> {{-- end of grid-wrap --}}
  @endif 

  @pagination


@endsection
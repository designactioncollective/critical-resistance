{{--  Slick carousel: https://kenwheeler.github.io/slick/  --}}
<section class="home-section--feature">

  <div class="slideshow">
    <?php if( have_rows('slides','option') ): while ( have_rows('slides','option') ) : the_row();?>
      @include('partials.cards.card-slide')
    <?php endwhile; endif;?>
  </div><!-- /.slideshow -->

  <div class="container">
    <div class="chapter-buttons">
      @foreach ($chapters as $chapter)
        <a href="<?= $chapter['chapter_link']['url']?>" target="<?= $chapter['chapter_link']['url']?>" class="btn-yellow">
          <span><?= $chapter['chapter_link']['title']?></span>
        </a>
      @endforeach
    </div><!-- /.chapter-container -->
  </div><!-- /.container -->

</section><!-- /.row -->

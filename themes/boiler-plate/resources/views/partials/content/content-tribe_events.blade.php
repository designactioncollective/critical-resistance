{{-- single events template --}}

<article @php(post_class())>
  <?php the_post_thumbnail('',['class' => 'card-img-top']); ?></a>
   <!-- Event Meta -->
   <?php do_action( 'tribe_events_before_the_meta' ) ?>
   <div class="tribe-events-event-meta">
       <div class="author <?php echo esc_attr( $has_venue_address ); ?>">

           <!-- Schedule & Recurrence Details -->
           <div class="tribe-event-schedule-details">
               <?php echo tribe_events_event_schedule_details() ?>
           </div>

           <?php if ( $venue_details ) : ?>
               <!-- Venue Display Info -->
               <div class="tribe-events-venue-details">
               <?php
                   $address_delimiter = empty( $venue_address ) ? ' ' : ', ';

                   // These details are already escaped in various ways earlier in the process.
                   echo implode( $address_delimiter, $venue_details );

                   if ( tribe_show_google_map_link() ) {
                       echo tribe_get_map_link_html();
                   }
               ?>
               </div> <!-- .tribe-events-venue-details -->
           <?php endif; ?>

       </div>
   </div><!-- .tribe-events-event-meta -->
   <?php do_action( 'tribe_events_after_the_meta' ) ?>


  {{-- content --}}
  <p class="card-text">@content</p>

  {{-- after content -- ical and map --}}
  <!-- .tribe-events-single-event-description -->
  <?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

  <!-- Event meta -->
  <?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
  <?php tribe_get_template_part( 'modules/meta' ); ?>
  <?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>

</article>

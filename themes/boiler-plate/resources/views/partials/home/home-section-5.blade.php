{{--  Home section  --}}
<section class="home-section--5">
  <div class="container">
    <div class="row">
      <h2 class="home-section__header"><a href="/resources">@text(Resources)</a></h2>

        {{--           --}}
        {{-- Resources --}}
        {{--           --}}

        @php
        // Variable that controls whether or not the displayed posts are
        // the most recent posts (true) or manually set posts (false)
        $manual_selection = get_field('resources_post_setting', 'option');
        $args = []; // declared before if statement to make sure variable scope is all good
        $featured_post = get_field('featured_resource_posts', 'option');
        @endphp

        {{-- Recent Post: --}}
        @if(!$manual_selection)

          @php
          $args = array(
            'post_type'      => array( 'resources' ),
            'posts_per_page' => 3,
          )
          @endphp

        {{-- Manually set post: --}}
        @else
          @php
          $args = array(
            'post_type' => 'any',
            'post__in'  => $featured_post,
            )
          @endphp
        @endif

        <?php global $post ?>
        @query($args)
          <div class="col-md-4">
            @include('partials.cards.card-home-c')
          </div>
        @endquery
        {{-- END  resources --}}

      </div>
    </div><!-- /.row -->
  </div><!-- /.container -->
</section>




<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    /***
     * Top Feature section on home page
     */
    public function featureImage()
    {
        return get_field('feature_image', 'option');
    }
    public function featureTitle()
    {
        return get_field('feature_title', 'option');
    }
    public function featureText()
    {
        return get_field('feature_text', 'option');
    }
    public function featureLink()
    {
        return get_field('feature_link', 'option');
    }
    public function chapters()
    {
        return get_field('chapters', 'option');
    }
    /***
     * Secondary Feature section on home page (Abolitionist)
     */
    public function secondaryImage()
    {
        return get_field('secondary_image', 'option');
    }
    public function secondaryText()
    {
        return get_field('secondary_text', 'option');
    }
    public function secondaryLink()
    {
        return get_field('secondary_link', 'option');
    }

    public function abolitionistLoop()
    {
        $resourceSidebarLoop = get_posts([
            'post_type'      => 'abolitionist',
            'posts_per_page' => '1',
        ]);

        return array_map(function ($post) {
            return [
                'link' => get_permalink($post->ID),
                'title' => $post->post_title
            ];
        }, $resourceSidebarLoop);
    }// Abolitionist
}

<?php

/**
*  Abolitionist custom post type
*/
// Register Custom Post Type
function custom_post_type_abolitionist() {

	$labels = array(
		'name'                  => _x( 'The Abolitionist', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Abolitionist', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Abolitionist', 'dac' ),
		'name_admin_bar'        => __( 'The Abolitionist', 'dac' ),
		'archives'              => __( 'The Abolitionist Archives', 'dac' ),
		'attributes'            => __( 'Abolitionist Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Abolitionist:', 'dac' ),
		'all_items'             => __( 'All The Abolitionist', 'dac' ),
		'add_new_item'          => __( 'Add New Item', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Abolitionist', 'dac' ),
		'edit_item'             => __( 'Edit Abolitionist', 'dac' ),
		'update_item'           => __( 'Update Abolitionist', 'dac' ),
		'view_item'             => __( 'View Abolitionist', 'dac' ),
		'view_items'            => __( 'View The Abolitionist', 'dac' ),
		'search_items'          => __( 'Search Abolitionist', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into item', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'dac' ),
		'items_list'            => __( 'Items list', 'dac' ),
		'items_list_navigation' => __( 'Items list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter items list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'Abolitionist', 'dac' ),
		'description'           => __( 'Post Type Description', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'menu_icon'  => 'dashicons-media-document',
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'abolitionist', $args );

}
add_action( 'init', 'custom_post_type_abolitionist', 0 );

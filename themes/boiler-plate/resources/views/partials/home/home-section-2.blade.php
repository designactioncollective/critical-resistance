{{--  Home section  --}}
<section class="home-section--2">
  <div class="container">
    <div class="row">

      {{-- News --}}
      <div class="col-sm-6">
          <h2 class="home-section__header"><a href="/news">@text(News)</a></h2>
          @php($args = array(
              'post_type'      => array( 'updates' ),
              'posts_per_page' => 2,
            ))
          @query($args)
            @include('partials.cards.card-home-b')
          @endquery
      </div>

      {{-- Events --}}
      <div class="col-sm-6">
        <h2 class="home-section__header"><a href="/cr-events">@text(Events)</a></h2>
        @events(2)
          @include('partials.cards.card-e')
        @endevents
      </div>

    </div><!-- /.row -->
  </div><!-- /.container -->
</section>




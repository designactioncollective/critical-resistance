<?php
/**
*  Resources custom post type
*/

/*		# Add some custom columns to the admin screen:
		'admin_cols' => [
			'resources_type' => [
				'taxonomy' => 'resource-type'
			],
			'resources_category' => [
				'taxonomy' => 'resource-category'
			],
			'resources_tag' => [
				'taxonomy' => 'resource-tag'
			],
		],
		*/

		// Register Custom Post Type
		function custom_post_type_resources() {

			$labels = array(
				'name'                  => _x( 'Resources', 'Post Type General Name', 'dac' ),
				'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'dac' ),
				'menu_name'             => __( 'Resources', 'dac' ),
				'name_admin_bar'        => __( 'Resource', 'dac' ),
				'archives'              => __( 'Item Archives', 'dac' ),
				'attributes'            => __( 'Item Attributes', 'dac' ),
				'parent_item_colon'     => __( 'Parent Item:', 'dac' ),
				'all_items'             => __( 'All Items', 'dac' ),
				'add_new_item'          => __( 'Add New Item', 'dac' ),
				'add_new'               => __( 'Add New', 'dac' ),
				'new_item'              => __( 'New Item', 'dac' ),
				'edit_item'             => __( 'Edit Item', 'dac' ),
				'update_item'           => __( 'Update Item', 'dac' ),
				'view_item'             => __( 'View Item', 'dac' ),
				'view_items'            => __( 'View Items', 'dac' ),
				'search_items'          => __( 'Search Item', 'dac' ),
				'not_found'             => __( 'Not found', 'dac' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
				'featured_image'        => __( 'Featured Image', 'dac' ),
				'set_featured_image'    => __( 'Set featured image', 'dac' ),
				'remove_featured_image' => __( 'Remove featured image', 'dac' ),
				'use_featured_image'    => __( 'Use as featured image', 'dac' ),
				'insert_into_item'      => __( 'Insert into item', 'dac' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'dac' ),
				'items_list'            => __( 'Items list', 'dac' ),
				'items_list_navigation' => __( 'Items list navigation', 'dac' ),
				'filter_items_list'     => __( 'Filter items list', 'dac' ),
			);
			$args = array(
				'label'                 => __( 'Resource', 'dac' ),
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'thumbnail' ),
				'taxonomies'            => array( 'resource-tag', 'resource-type', 'resource-category' ),
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
				'menu_position'         => 5,
				'menu_icon'             => 'dashicons-admin-tools',
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capability_type'       => 'page',
				'show_in_rest'          => true,
			);
			register_post_type( 'resources', $args );

		}
		add_action( 'init', 'custom_post_type_resources', 0 );

/**
* Custom taxonomy:  Type
*/
// Register Custom Taxonomy
function custom_taxonomy_resource_type() {

	$labels = array(
		'name'                       => _x( 'Resource Types', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Resource Type', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Resource Types', 'dac' ),
		'all_items'                  => __( 'All Resource Type', 'dac' ),
		'parent_item'                => __( 'Parent Item', 'dac' ),
		'parent_item_colon'          => __( 'Parent Item:', 'dac' ),
		'new_item_name'              => __( 'New Item Name', 'dac' ),
		'add_new_item'               => __( 'Add New Item', 'dac' ),
		'edit_item'                  => __( 'Edit Item', 'dac' ),
		'update_item'                => __( 'Update Item', 'dac' ),
		'view_item'                  => __( 'View Item', 'dac' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Items', 'dac' ),
		'search_items'               => __( 'Search Items', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No items', 'dac' ),
		'items_list'                 => __( 'Items list', 'dac' ),
		'items_list_navigation'      => __( 'Items list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'resource-type', array( 'resources' ), $args );

}
add_action( 'init', 'custom_taxonomy_resource_type', 0 );


/**
* Custom taxonomy:  Resource Category
*/
// Register Custom Taxonomy
function custom_taxonomy_resource_category() {

	$labels = array(
		'name'                       => _x( 'Resource Categories', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Resource Category', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Resource Categories', 'dac' ),
		'all_items'                  => __( 'All Resource Category', 'dac' ),
		'parent_item'                => __( 'Parent Item', 'dac' ),
		'parent_item_colon'          => __( 'Parent Item:', 'dac' ),
		'new_item_name'              => __( 'New Item Name', 'dac' ),
		'add_new_item'               => __( 'Add New Item', 'dac' ),
		'edit_item'                  => __( 'Edit Item', 'dac' ),
		'update_item'                => __( 'Update Item', 'dac' ),
		'view_item'                  => __( 'View Item', 'dac' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Items', 'dac' ),
		'search_items'               => __( 'Search Items', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No items', 'dac' ),
		'items_list'                 => __( 'Items list', 'dac' ),
		'items_list_navigation'      => __( 'Items list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'resource-category', array( 'resources' ), $args );

}
add_action( 'init', 'custom_taxonomy_resource_category', 0 );


/**
* Custom taxonomy:  Resource Tags
*/
// Register Custom Taxonomy
function custom_taxonomy_resource_tag() {

	$labels = array(
		'name'                       => _x( 'Resource Tags', 'Taxonomy General Name', 'dac' ),
		'singular_name'              => _x( 'Resource Tag', 'Taxonomy Singular Name', 'dac' ),
		'menu_name'                  => __( 'Resource Tags', 'dac' ),
		'all_items'                  => __( 'All Resource Tag', 'dac' ),
		'parent_item'                => __( 'Parent Item', 'dac' ),
		'parent_item_colon'          => __( 'Parent Item:', 'dac' ),
		'new_item_name'              => __( 'New Item Name', 'dac' ),
		'add_new_item'               => __( 'Add New Item', 'dac' ),
		'edit_item'                  => __( 'Edit Item', 'dac' ),
		'update_item'                => __( 'Update Item', 'dac' ),
		'view_item'                  => __( 'View Item', 'dac' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'dac' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'dac' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
		'popular_items'              => __( 'Popular Items', 'dac' ),
		'search_items'               => __( 'Search Items', 'dac' ),
		'not_found'                  => __( 'Not Found', 'dac' ),
		'no_terms'                   => __( 'No items', 'dac' ),
		'items_list'                 => __( 'Items list', 'dac' ),
		'items_list_navigation'      => __( 'Items list navigation', 'dac' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'resource-tag', array( 'resources' ), $args );

}
add_action( 'init', 'custom_taxonomy_resource_tag', 0 );

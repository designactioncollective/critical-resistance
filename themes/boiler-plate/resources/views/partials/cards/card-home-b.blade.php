{{--
  Card B:
  Home -- Text only
--}}
<article @php(post_class('card-home-b'))>


  <div class="card-body">
    @include('partials/entry-meta')
    <h3><a href="@permalink">@title</a></h3>
    {{-- <p class="card-text">@excerpt</p> --}}
    <!-- <a href="#" class="btn btn-primary">Card Button</a> -->
    <!-- <a href="#" class="card-link">Card link</a> -->

  </div><!-- card-body -->

</article><!-- card -->

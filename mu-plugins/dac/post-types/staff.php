<?php
/**
* Staff custom post type
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Staff & Attorney Custom Post Types
function dac_custom_post_type_staff() {

	// Staff and Attorney
	$labels = array(
		'name'                  => _x( 'Staff', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Staff', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Staff', 'dac' ),
		'name_admin_bar'        => __( 'Staff', 'dac' ),
		'archives'              => __( 'Staff Archives', 'dac' ),
		'parent_item_colon'     => __( 'Parent Staff:', 'dac' ),
		'all_items'             => __( 'All Staff', 'dac' ),
		'add_new_item'          => __( 'Add New Staff', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Staff', 'dac' ),
		'edit_item'             => __( 'Edit Staff', 'dac' ),
		'update_item'           => __( 'Update Staff', 'dac' ),
		'view_item'             => __( 'View Staff', 'dac' ),
		'search_items'          => __( 'Search Staff', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into Staff', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Staff', 'dac' ),
		'items_list'            => __( 'Staff list', 'dac' ),
		'items_list_navigation' => __( 'Staff list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter Staff list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'Staff', 'dac' ),
		'description'           => __( 'Staff', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'revisions', 'page-attributes', 'custom-fields', 'title' ),
	//	'taxonomies'            => array('role'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'staff', $args );

}
add_action( 'init', 'dac_custom_post_type_staff', 0 );


// Change "Enter Title Here" to "Full Name"
add_filter( 'enter_title_here', function($title){
     $screen                   =  get_current_screen();

     if  ( 'staff'            == $screen->post_type ) {
          $title               =  'Full Name';
     }

     return $title;
});


/*
 * Add columns to staff post list
 */
// add_filter ( 'manage_staff_posts_columns', function($columns){
// 	return array_merge ( $columns, array (
// 		'people_order' => __ ( 'Order' )
// 	  ) );
// });

 /*
 * Add columns to member post list
 */
// add_action ( 'manage_staff_posts_custom_column', function($column, $post_id){
// 	switch ( $column ) {
// 		case 'people_order':
// 		  echo get_post_meta ( $post_id, 'people_order', true );
// 		  break;
// 	  }
// }, 10, 2);



/*
 * Make cust col sortable
 */
// add_filter( 'manage_edit-staff_sortable_columns', function(){
// 	$columns['people_order'] = 'people_order';
// 	return $columns;
// });

// add_action( 'pre_get_posts', function($query){
// 	if ( ! is_admin() )
// 	return;

// 	$orderby = $query->get( 'orderby');

// 	if ( 'people_order' == $orderby ) {
// 		$query->set( 'meta_key', 'people_order' );
// 		$query->set( 'orderby', 'meta_value_num' );
// 	}
// });




 /*
 * Show all staff and board members on archive page
 */
// function show_all_staff_and_board( $query ) {
//   if(
// 	  $query->is_main_query() &&
// 	  !is_admin() &&
// 		(
// 			is_post_type_archive('staff' ) ||
// 			is_post_type_archive('board' )
// 		)
// 	) {
// 		$query->set( 'posts_per_page', '-1' );

// 		$query->set(
// 			'meta_query' , array(
// 				'relation' => 'OR',
// 					array(
// 						'key' => 'people_order',
// 						'value' => '0',
// 						'compare' => '>='
// 					),
// 					array(
// 						'key' => 'people_order',
// 						'value' => '0', // This is ignored, but is necessary...
// 						'compare' => 'NOT EXISTS', // works!
// 					)
// 				)
// 			);
// 		$query->set('meta_key', 'people_order');
// 		$query->set('orderby', 'meta_value_num title'); # Sorting works with meta_value as well as meta_value_num - I've tried both
// 		$query->set('order', 'ASC');

//     }
//   }
// add_action( 'pre_get_posts', 'show_all_staff_and_board' );

/*
 * Make sure all people have a value for people_order ACF
 */
// function populate_empty_people_order( $post_id ) {

// 	// If this is just a revision, don't send the email.
// 	if ( (get_post_type( $post_id ) != 'staff') ||
// 		 (get_post_type( $post_id ) != 'board'))
// 		return;

// 	$people_order = get_field('people_order',$post_id);

// 	if(!$people_order) { // if value for people order DNE, update it to 999
// 		update_post_meta( $post_id, 'people_order', '999');
// 	}
// }
// add_action( 'save_post', 'populate_empty_people_order' );

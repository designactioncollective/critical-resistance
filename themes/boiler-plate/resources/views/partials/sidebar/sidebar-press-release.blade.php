@if (is_post_type_archive('press-release') || is_singular('press-release'))
  <?php $args = array(
    'type'            => 'monthly',
    'limit'           => '',
    'format'          => 'option',
    'before'          => '',
    'after'           => '',
    'show_post_count' => false,
    'echo'            => 1,
    'order'           => 'DESC',
    'post_type'       => 'press-release'
  );?>

  {{-- Date archive select --}}
  <section>
    <h3>@text(Sort by Date)</h3>
    <div class="select-wrap">
      <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
        <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option>
        <?php wp_get_archives( $args ); ?>
      </select>
    </div><!-- /.select-wrap -->
  </section>

  {{-- Search Press Releases --}}
  <section>
    <h3>@text(Search Press Releases)</h3>
    <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" class="form-inline">
      <input type="text" name="s" placeholder="Search Press Releases"/>
      <input type="hidden" name="post_type" value="press-release" /> <!-- // hidden 'products' value -->
      <input type="submit" alt="Search" value="Search" />
    </form>
  </section>

@endif

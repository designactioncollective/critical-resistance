/**
 * Custom JS added by DAC
 *
 */

// import external dependencies
import 'jquery';
import $ from 'jquery';

/**
 * DAC: Add Slimmenu
 * https://github.com/adnantopal/slimmenu
 */
import 'slimmenu/dist/js/jquery.slimmenu.min.js';

$('.slimmenu').slimmenu(
  {
      resizeWidth: '767', /* Navigation menu will be collapsed when document width is below this size or equal to it. */
      initiallyVisible: false, /* Make main navigation menu initially visible on mobile devices without the need to click on expand/collapse icon. */
      collapserTitle: '<span class="sr-only">Main Menu</span>', /* Collapsed menu title. */
      animSpeed: 'fast', /* Speed of the sub menu expand and collapse animation. */
      easingEffect: null, /* Easing effect that will be used when expanding and collapsing menu and sub menus. */
      indentChildren: false, /* Indentation option for the responsive collapsed sub menus. If set to true, all sub menus will be indented with the value of the option below. */
      childrenIndenter: '&nbsp;', /* Responsive sub menus will be indented with this character according to their level. */
      expandIcon: '<i class="fa fa-angle-down"></i>', /* An icon to be displayed next to parent menu of collapsed sub menus. */
      collapseIcon: '<i class="fa fa-angle-up"></i>', /* An icon to be displayed next to parent menu of expanded sub menus. */
  });

  $(".collapse-button").on("click", function () {
    $(this).toggleClass("active");
  });

/**
 * DAC: Add Slick Carousel
 * https://github.com/kenwheeler/slick
 */
import 'slick-carousel/slick/slick.min.js';

$(document).on('ready', function () {

  // Main slideshow
  $(".slideshow").slick({
      dots: true,
      accessibility: true,
      arrows: true,
      nextArrow: '<button type="button" class="slick-next">Next</button>',
      prevArrow: '<button type="button" class="slick-prev">Previous</button>',
      infinite: true,
  }); // slideshow

  /**
   * Hide search form if click not in form
   */

  $(document).mouseup(function(e)
  {
    var container = $(".search-form");

    // if the target of the click isn't the container nor a descendant of the container
    if (
      (!container.is(e.target) && container.has(e.target).length === 0)
      && (container).is(":visible")
      && ($(e.target).attr('class') != 'h-search-icon')
    )
    {
        $('.search-accordion a').attr("aria-expanded","false");
        $('.search-accordion a').addClass("collapsed");
        $('#h-search-accordion').removeClass("show");

        console.log($(e.target).attr('class'));
    }
  }); // mouse up

}); // doc ready


/**
 * Text re-sizer
 * Thanks to --> https://davidwalsh.name/change-text-size-onclick-with-javascript
 */
$("#plus-text").on("click", function(e) {
  e.preventDefault();
  resizeText(1);
});
$("#minus-text").on("click", function(e) {
  e.preventDefault();
  resizeText(-1);
});
$("#default-text").on("click", function(e) {
  e.preventDefault();
  resizeText('');
});
function resizeText(multiplier) {
  if (document.body.style.fontSize == "") {
    document.body.style.fontSize = "1.0em";
  }
  if(multiplier) {
    document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 0.2) + "em";
  } else {
    document.body.style.fontSize = "1.0em";
  }
}// end text resizer

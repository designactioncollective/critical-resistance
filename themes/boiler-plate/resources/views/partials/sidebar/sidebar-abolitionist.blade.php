<aside>
  <section>
    <h3>@text(Team)</h3>
    <?php if( have_rows('team','option') ): while ( have_rows('team','option') ) : the_row();?>

      <div class="abolitionist-side__meta">
        
        <div class="abolitionist-side__name">
          <?php the_sub_field('name');?>
        </div> 

        <div class="abolitionist-side__position">
          <?php the_sub_field('position');?>
        </div> 

      </div><!-- /.aboltionist__meta -->

    <?php endwhile; endif;?>


    <?php if( have_rows('cards','option') ): while ( have_rows('cards','option') ) : the_row();?>

      <div class="abolitionist-side__card <?=  get_sub_field('content') ? 'with-content' : '';?>">
        @if(get_sub_field('content'))
          <div class="abolitionist-side__content">
            <?php the_sub_field('content');?>
          </div> 
        @endif

        @if(get_sub_field('link'))
          <?php $link = get_sub_field('link'); ?>
          <a class="btn abolitionist-side__link btn-red btn-sidebar" href="<?= $link['url']?>" target="<?= $link['url']?>"><?= $link['title']?></a>
        @endif
        
      </div><!-- /.abolitionist-side__card -->
    <?php endwhile; endif;?>


  </section>
</aside>
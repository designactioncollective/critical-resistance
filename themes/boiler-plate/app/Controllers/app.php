<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }
    /*
    ===============================================================================
    *  DAC MODIFICATIONS BELOW
    ===============================================================================
    */

    public function socialMedia()
    {
        return get_field('social_media_links', 'option');
    }

    public function address()
    {
        return get_field('address', 'option');
    }

    public function email()
    {
        return get_field('email', 'option');
    }

    public function phone()
    {
        return get_field('phone', 'option');
    }

    public function donate()
    {
        return get_field('donate', 'option');
    }
     /***
     * Donation Footer
     */
    public function donationHeadline()
    {
        return get_field('donation_headline', 'option');
    }
    public function donationText()
    {
        return get_field('donation_text', 'option');
    }
    public function donationButton()
    {
        return get_field('donation_button', 'option');
    }
    /*
    * Remove lables from archive feed titles
    */
    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if ( is_category() ) {
            return single_cat_title( '', false );
        }
        if ( is_tag() ) {
            return single_tag_title( '', false );
        }
        if ( is_singular('tribe_events') ) {
            global $wp_query;
            return get_the_title($wp_query->queried_object->ID);
        }
        if ( is_author() ) {
            return '<span class="vcard">' . get_the_author() . '</span>';
        }
        if ( is_post_type_archive() ) {
            return post_type_archive_title( '', false );
        }
        if ( is_tax() ) {
            return single_term_title( '', false );
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
}

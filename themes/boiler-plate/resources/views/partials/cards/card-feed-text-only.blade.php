{{--
  Text only cards
--}}
<article @php(post_class('card-feed-text-only'))>

  <div class="card-body">

    @if(get_post_type() == 'updates') 
      <div class="chapter-tags"><?= get_the_tag_list( '', ', ', '' ); ?></div>
    @endif 

    @if(get_post_type() == 'tribe_events') 
      <time><?= tribe_get_start_date( $post, false, 'F j, Y - g:i a'); ?> </time>
      <address><?= tribe_get_city($post->ID);?></address>
    @else
      <time class="date" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
    @endif

    <a href="@permalink" >
      <h3 class="card-title"><a href="@permalink">@title</a></h3>
    </a>

  </div><!-- card-body -->

</article><!-- card -->

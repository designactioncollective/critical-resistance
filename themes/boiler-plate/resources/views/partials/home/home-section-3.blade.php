{{--  Home section  --}}
<section class="home-section--3">
  <div class="container">
    <div class="row">

      <article class='home-abolitionist'>
        @if($secondary_link)
          <a href="<?= $secondary_link['url']?>" target="<?= $secondary_link['target']?>" class="img-link">
        @endif

        @php
          echo wp_get_attachment_image( $secondary_image, 'square' );
        @endphp

        @if($secondary_link)
          </a>
        @endif

        <div class="card-body"><p>{!! $secondary_text!!}</p>
          <br>
          @if($abolitionist_loop)
            @foreach($abolitionist_loop as $abolitionist)

              <a href="{!!$abolitionist['link']!!}" class="btn btn-red">
                <span>@text(Latest Issue: ){{ $abolitionist['title'] }}</span>
              </a>
            @endforeach
          @endif

        </div><!-- card-body -->

      </article><!-- card -->


    </div><!-- /.row -->
  </div><!-- /.container -->
</section>




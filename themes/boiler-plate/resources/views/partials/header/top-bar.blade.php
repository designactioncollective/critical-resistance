<section class="top-bar">
  <div class="container">
    <div class="row">

      {{-- TOP LEFT --}}
      <div class="col top-bar__left">
        {{-- Logo: set in customizer --}}
        @php 
        //$logo = (is_home() ? 'themeslug_logo_home' : 'themeslug_logo_internal');
        $logo = 'themeslug_logo_internal';
        @endphp

        <div class='site-logo'>
            <a href='{{ home_url('/') }}' title='{{ get_bloginfo('name', 'display') }}' rel='home'>
              <img src='{{ get_theme_mod( $logo )}}' class='img-fluid' alt='{{get_bloginfo( 'name', 'display' )}}'>
            </a>
        </div>


      </div>

      {{-- TOP RIGHT --}}
      <div class="col top-bar__right">

        <div class="top-bar__right--icons">
          @include('partials.search-expand-button')
          @include('partials.social-media')
        </div>

        <button type="button" class="btn btn-updates" data-toggle="modal" data-target="#getUpdates">
            <span>@text(Get Updates)</span>
        </button>

        <a href="{!! $donate['url'] !!}" target={!! $donate['target']!!} class="btn-donate"><span>{!! $donate['title']!!}</span></a>

      </div>{{-- top-bar__right --}}

    </div><!-- /.row -->
  </div><!-- container -->
</section><!-- top-bar -->

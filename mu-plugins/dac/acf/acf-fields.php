<?php
/**
* Place ACF JSON in field-groups directory
*
* Helpful for syncing ACF fields with git.
* https://www.advancedcustomfields.com/resources/local-json/
*/               

// Save JSON data     
add_filter('acf/settings/save_json', function( $path ) {
  
    // update path
    $path = dirname(__FILE__) . '/field-groups';
    
    // return
    return $path;   
});

// Load JSON data
add_filter('acf/settings/load_json', function( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);
    
    // append path
    $paths[] = dirname(__FILE__) . '/field-groups';
    
    // return
    return $paths;
    
});

@php 
//Vars
$banner_image_id             = '';
$archive_banner_enable_field = 'enable_custom_banner_image-' . get_post_type();
$archive_banner_image_field  = 'banner_image-' . get_post_type();

//Custom page banner takes priority 
if(get_field('enable_custom_banner_image') && get_field('banner_image')) {
  //Custom page banner 
  $banner_image_id = get_field('banner_image');
}

//Else if archive page banner 
elseif(get_field($archive_banner_enable_field, 'option') && get_field($archive_banner_image_field, 'option'))  {
  //Archive page banner
  $banner_image_id = get_field($archive_banner_image_field, 'option');
}

//Fall back to random image
else {
  // Random image from theme options
  $array_of_default_images = [];
  if( have_rows('default_banner_images','option') ):
      while ( have_rows('default_banner_images','option') ) : the_row();
        array_push($array_of_default_images,get_sub_field('default_banner_image'));
      endwhile;
      // get random index from array 
      $banner_image_id =  $array_of_default_images[array_rand($array_of_default_images)];

  else :
    echo '<div class="container"><div class="alert alert-warning" role="alert">';
    echo 'Please add at least one default banner image.';
    echo '</div></div>';
  endif;
}


@endphp

<header class="page-header" style="background-image:url('<?= wp_get_attachment_image_src($banner_image_id,'slide')[0]?>')">


{{-- </header> this tag is closed in page-header.blade.php --}}